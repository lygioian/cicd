FROM node:12.18.2-alpine

WORKDIR /ctxh

COPY package*.json ./

RUN npm install

COPY . .

LABEL maintainer="lygioian@gmail.com"

EXPOSE 3030
#development
CMD ["yarn","release"]