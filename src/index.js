import { server, app } from './app';
import Database from './database';
import { configureRoutes } from './routes';
// import { configureCmsRoutes } from './cms/routes';
import config from './config';

// Config routes
configureRoutes(app);

//config cms routes
// configureCmsRoutes(app);

// Connect to db
console.log('Connecting to database ...');

var db = Database.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', () => {
    server.listen(process.env.PORT || 3000, process.env.HOST || '::',  function (err) {
        if (err) console.log(err)
        console.log(`server listening on ${server.address().port}`)
      });
    console.log('Connection has been established successfully.'); 

    // for run intergrate test only
    app.emit('ready');
});

export default app;