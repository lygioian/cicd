import { Router } from 'express';
import { verifyJWT_MW } from '../services/auth.service';
import { filterLocation } from '../services/filter.service';
import { filterPaginator } from '../services/filter.service';
import { uploadImageIntoServer, changeUploadImageIntoServer } from '../services/upload.service';
// import * as Notification from '../services/notification.service';
import * as NewsService from '../services/news.service';
import * as UserService from '../services/user.service';
// import * as PlaceService from '../services/place.service';
import { ServerEventSystem } from '../server-events';

import _ from 'lodash';

const router = Router();

/**
 * Filter BEFORE handling
 */
router.all('*', verifyJWT_MW);

// router.get('/', (req, res) => {
    // let key = _.head(_.keys(req.query));
    
    // switch(key) {
    //     case 'place':
    //         if(req.query.user) {
    //             getPostAtLocationByUser(req, res);
    //         }
    //         else if(req.query.active) {
    //             getPostByLocation(req, res)
    //         }
    //         else {
    //             getPostByLocation(req, res)
    //         }
    //         break;
    //     default: 
            // getAllPost(req, res);
    // }
// });

/**
 * Get all post
 */

router.get('/', (req, res) => {
    getAllPost(req, res);
});

function getAllPost(req, res) {
    NewsService.getAll().populate('userCreated').then(data => {
        data = _.sortBy(data, [(e) => { return parseInt(e.createdAt) * -1 }]);
        res.send(data);
    });
 }

/**
 * Upload Image in there
 */
router.post('/', uploadImageIntoServer);

/**
 * Create news
 */
router.post('/', (req, res) => {
    req.body.userCreated = req.user._id;
    req.body.media = {
        type: req.fileUploaded.type,
        path: { 
            original: req.fileUploaded.path, 
            thumbnail: req.fileUploaded.thumbnailPath,
            small: req.fileUploaded.smallPath,
        }
    };

    // req.body.friends = req.body.friends != "" &&  req.body.friends !== undefined ? req.body.friends.split(",") : [];
    NewsService.add(req.body).then(post => post).then(post => {
        res.send(post);
    }).catch(error => {
        res.status(202).send({error: error.message});
    })
});

/**
 * Delete specific news
 */
router.delete('/:newsId', (req, res) => {
    NewsService.remove(req.params.newsId, req.user._id).then(post => {
        if(!post) {
            res.status(202).send({error: "Cannot excuted. Event not exist or you not created it."});
        }
        res.send(post)
    }).catch(error => {
        res.status(400).send(error);
    })
})

/**
 * Upload Image in there
 */
router.patch('/:id', changeUploadImageIntoServer);

/*
* Update news info
*/
router.patch('/:id', (req, res) => {
    if(req.files !== null)
        req.body.media = {
            type: req.fileUploaded.type,
            path: { 
                original: req.fileUploaded.path, 
                thumbnail: req.fileUploaded.thumbnailPath,
                small: req.fileUploaded.smallPath,
            }
        };
    console.log(req.body)
    NewsService.editOne(req.params.id, req.body).then(data => {
        res.send(data);
    }).catch(error => {
        res.status(400).send({error: "Cannot update. No resource exist"});
    })
});


/**
 * Get post at location by user
 */

function getPostAtLocationByUser(req, res) {
    NewsService.locationPostByUser(req.query.place, req.query.user).then(post => {
        res.send(post)
    }).catch(error => {
        res.status(400).send(error)
    })
}

/**
 * Get posts by user
 */
router.get('/userlead', (req, res) => {
    NewsService.postOfLeader(req.user._id).then(data => {
        data = _.sortBy(data, [(e) => { return parseInt(e.createdAt) * -1 }]);
        res.send(data);
        
    }).catch(error => {
        res.status(400).send(error);
    })
 })
 
 router.get('/usercreated', (req, res) => {
    NewsService.postByUser(req.user._id).then(data => {
        data = _.sortBy(data, [(e) => { return parseInt(e.createdAt) * -1 }]);
        res.send(data);
        
    }).catch(error => {
        res.status(400).send(error);
    })
 })


/**
 * Get post detail
 */
router.get('/:postId', (req, res) => {
    NewsService.getOne(req.params.postId).then(data => {
        data ? res.send(data) : res.status(404).send({error: "Post is not exist."});
    }).catch(error => {
        res.status(400).send({error: "Post ID is not correct or not exist"});
    })
})

// /**
//  * Register specific post
//  */
router.post('/:postId/register', (req, res) => {
    NewsService.registerEvent(req.params.postId, req.user._id).then(post => {
        
        if(!post) {
            res.status(202).send({error: "Cannot excuted. User haved been liked yet."});
        }
        else {
            res.send(post);

            // Notification.generateMessage('post', 'like', req.user._id, post._id, post.userPost, false, post).then(data => {
            //     if(data) ServerEventSystem.notifyUser(post.userPost, data);
            // })
        }
    }).catch(error => {
        res.status(400).send(error);
    })
})


/**
 * Unlike specific post
 */
router.delete('/:postId/register', (req, res) => {
    NewsService.unRegisterEvent(req.params.postId, req.user._id).then(post => {
        if(!post) {
            res.status(202).send({error: "Cannot excuted. User have never been liked yet."});
        }

        res.send(post)
    }).catch(error => {
        res.status(400).send(error);
    })
})

router.post('/:postId/:userId/checkin', (req, res) => {
    NewsService.checkinEvent(req.params.postId, req.params.userId).then(post => {
        
        if(!post) {
            res.status(202).send({error: "Cannot excuted. User haved been liked yet."});
        }
        else {
            res.send(post);

            // Notification.generateMessage('post', 'like', req.user._id, post._id, post.userPost, false, post).then(data => {
            //     if(data) ServerEventSystem.notifyUser(post.userPost, data);
            // })
        }
    }).catch(error => {
        res.status(400).send(error);
    })
})


/**
 * Unlike specific post
 */
router.delete('/:postId/:userId/checkin', (req, res) => {
    NewsService.deleteUserCheckinEvent(req.params.postId, req.params.userId).then(post => {
        if(!post) {
            res.status(202).send({error: "Cannot excuted. User have never been liked yet."});
        }

        res.send(post)
    }).catch(error => {
        res.status(400).send(error);
    })
})

// /**
//  * Addleader specific post
//  */
router.post('/:postId/:userId/addlead', (req, res) => {
    NewsService.addLeader(req.params.postId, req.params.userId).then(post => {
        
        if(!post) {
            res.status(202).send({error: "Cannot excuted. User haved been liked yet."});
        }
        else {
            res.send(post);

            // Notification.generateMessage('post', 'like', req.user._id, post._id, post.userPost, false, post).then(data => {
            //     if(data) ServerEventSystem.notifyUser(post.userPost, data);
            // })
        }
    }).catch(error => {
        res.status(400).send(error);
    })
})


/**
 * Unlike specific post
 */
router.delete('/:postId/:userId/addlead', (req, res) => {
    NewsService.deleteLeader(req.params.postId, req.params.userId).then(post => {
        if(!post) {
            res.status(202).send({error: "Cannot excuted. User have never been liked yet."});
        }

        res.send(post)
    }).catch(error => {
        res.status(400).send(error);
    })
})


export default router;