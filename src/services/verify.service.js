import Event from "../models/event.model";
import _ from "lodash";

export async function verifyAdmin(req, res, next) {
  try {
    if (req.user.role == "user")
      res.status(202).send({ message: "Permission Denied" });
    next();
  } catch (err) {
    res.send(err);
  }
}

export async function verifyUserCreated(req, res, next) {
  try {
    Event.findById(req.params.eventId).then((event) => {
      if (event.userCreated != req.user._id)
        res.status(202).send({ message: "Permission Denied" });
      next();
    });
  } catch (err) {
    res.send(err);
  }
}
export async function verifyLeader(req, res, next) {
  try {
    // need to embed role in token, data query cost a lotsssss
    let userId = req.user._id;
    let event = await Event.findById(req.params.eventId);
    if (event.userStaff.length < 1) {
      return res.status(404).send({ message: "There is no staff" });
    }
    let staffs = event.userStaff;
    // lodash have async loop
    let staff = await _.find(staffs, function (user) {
      return user.user == userId;
    });

    if (!staff || staff.role != "Leader") {
      return res.status(202).send({ message: "Permission Denied" });
    }
    next();
  } catch (err) {
    res.status(400).send({ message: "Bad request", err: err });
  }
}
export async function verifyStaff(req, res, next) {}
