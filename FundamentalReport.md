# ***Exercises answer***
## *Fundamental Training Working Environment*

### **Knowledge management**
- All documents from now on should be created using markup languages (Markdown, AsciiDoc, PlantUML), Why should we do that ?
```javascript
Markup language also provides simple syntax that everyone in a team can easily know by a given rule. The simple format saves a significant amount of time when thinking about formatting and aesthetics. With markup language you can focus on the content, not on the shape. It speeds up the workflow of everything, from development to management tasks. Moreover, it can work on any plain text editor and it is also supported to embed to many platforms.
```
### **Regular Expression**
#### **1. Check if a string supported card number**
There are many kinds of card numbers. I have proposed 2 types: International Card and Domestic card. We will create a function that check validation for international card from the following table:

| International Card | Description |
|:-|:-|
| Visa | All Visa card numbers start with a 4. New cards have 16 digits. Old cards have 13. |
| MasterCard | MasterCard numbers either start with the numbers 51 through 55 or with the numbers 2221 through 2720. All have 16 digits. |
| JCB | JCB cards beginning with 2131 or 1800 have 15 digits. JCB cards beginning with 35 have 16 digits. |

```javascript 
/*Create a function to check valid card number. In this case, we just support 3 kind: visa, mastercard and jcb. 
Input: This function receive 2 param: type (a string that mean type of input card number), cardNumber (a string contain the number of card)
Output: return a boolean value true mean valid, false mean invalid*/
function checkValidInternationalCardNumber(type, cardNumber) {
    /*List of regex string for each type of international card.
    ?: in regex means non-capturing group. Just to identify, which means that the substring matched by that group will not be included in the list of captures.*/
    let regexString = {
        visa: /^4[0-9]{12}([0-9]{3})?$/g,
        mastercard: /^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$/g,
        jcb: /^(?:2131|1800|35\d{3})\d{11}$/g
    }
    return cardNumber.match(regexString[type]) != null
}
```
#### **2. Check if a string is a valid VNG employee email**
VNG also provide an personal email for each employee with the following rule:
```sh
<Domain>@vng.com.vn
```
Domain is also an important string that we need to remember, it can be defined with 
```sh
<your_name><first letter of each last name and middle names><number>
```
For example: 
```sh
Ly Gioi An -> anlg
```
However, in some cases if the domain exists, it will combine the number which will automatically increase, like anlg1, anlg2, anlg3,... 

```javascript
/*Create a function to check valid employee email. 
Input: This function receive a param: email that contain a string that need to verify
Output: return a boolean value true mean valid, false mean invalid*/
function checkValidEmailVNGEmployeeEmail(email) {
    // Regex string to verify the VNG employee email
    let regexString = /^[a-z]+[0-9]*@vng.com.vn$/g
    return email.match(regexString) != null
}
```

#### **3. Extract OTP from bank's text messages**

When I used ZaloPay, I found that there are 2 kind of the OTP response:
```re  
1. ZaloPay: Ma OTP de xac nhan cap lai mat khau thanh toan (MKTT) la 360484. KHONG CHIA SE MA OTP/MKTT VOI BAT KI AI. Neu KHONG thuc hien, lien he: 1900545436.
2. ZaloPay: Ban dang xac thuc so dien thoai de tao tai khoan, nhap ma OTP 505229 de xac thuc. KHONG CHIA SE MA OTP VOI BAT KI AI.
```
Now, I will use this model for bank's text messages.
We know that OTP also has 6 digits in our definition. The problem is how to detect 2 finds of OTP. In the first case it also has 6 characters and the dot in a word, second case it only contains 6 digits in a word, the idea is to detect the first word that has only 6 digits.

```javascript
/*Create a function to get OTP from the message. 
Input: This function receive a param: textInput that contain a string that contain the OTP
Output: return a OTP from the textInput message*/
function extractOTPFromBankMessage(textInput) {
    // Regex string to verify the OTP 
    let regexString = /[0-9]{6}/g
    return textInput.match(regexString)[0]
}
```
#### **4. Convert camel-case names to normal name**

```javascript
/*Create a function to get OTP from the message. 
Input: This function receive a param: camelcase that contain a camel case string
Output: return a normal string from camel case*/
function convertCamelcaseToNormalString(camelcase) {
    // Regex string to verify the the upper case 
    let regexString = /([A-Z])/g
    return camelcase.replace(regexString, " $1" );
}
```
### **Unit testing**
### **Docker**
#### **1. Different between Docker and Virtual machine**
First, Docker is contianer based technology and containers are just user space of the operating system, it is a process that run on our system. A container just a set of processes that are isolated from the rest of the system, running from a distinct image that provides all files necessary to support the processes. It's build for running applications. <br>
On the other hand, a Virtual Machine are made up of user space and kernel space of an operating system. The server hardware is virtualized, each Virtual Machine has OS and apps. <br>
-> They similar in that they both provide isolated environments. However, containers are typically much smaller and faster. The trade-off is that containers don't do true virtualization; you can't run a windows container on a Linux host for example.
#### **2. Different between Docker RUN and CMD**
RUN lets us to execute commands inside of our Docker image. These commands get executed once at build time and get written into our Docker image as a new layer. It also use to install our application and packages required for it. <br>
For example, if you wanted to install a package or create a directory inside of your Docker image then RUN will be that we will use. 
```
RUN mkdir -p /path/folder/example
```
CMD lets us define a default command to run when our container starts. It will be executed only when we run container without specifying a command. If Docker container runs with a command, the default command will be ignored.
#### **3. Different between Docker ENV and ARG**
ENV is mainly meant to provide default values for our future environment variables. Running dockerized applications can access and edit the environment variables.<br>
ARG values are not available after the image is built, just like RUN. A running container won't have access to an ARG variable value. ARG is value that only access before build an image and in the process when you build an image.
#### **4. Different between Docker COPY and ADD**
COPY only lets you copy in a local file or directory from your host into the Docker image. <br>
ADD lets us to copy in a local file or directory like COPY too, but it also support 2 other sources. First, you can use a URL instead of a local file/directory. Secondly, you can extract a tar file from the source directly into the destination

Dockerfile
```docker
FROM node:12.18.2-alpine

WORKDIR /ctxh

COPY package*.json ./

RUN npm install

COPY . .

LABEL maintainer="lygioian@gmail.com"

EXPOSE 3030
#development
CMD ["yarn","release"]
```
To run this docker
```docker
docker build . #use to build an image
docker -d run <imageId> #To build container with the image id, -d mean it will run the process background until we kill it.
```

Or the can use the docker compose
```docker
version: "1.0"
services:
    node:
        container_name: node_CTXH
        restart: always
        build:
            context: .
            dockerfile: ./docker/Dockerfile
        volumes:
            - ./upload:/ctxh/upload
        command: yarn release
        networks:
            - common
    nginx:
        container_name: nginx
        image: nginx
        ports:
            - 3456:90
        volumes:
            - ./nginx/conf.d/:/etc/nginx/conf.d
        networks:
            - common
```
We use docker compose when we want to build 2 or more container in a project with one command.
```docker
docker-compose build -> Build or rebuild services
docker-compose up -> Create and start containers
docker-compose down -> Stop and remove containers, networks, images, and volumes
```
To publish new tag to docker hub we can build the image and push to the docker hub by this command (Obviously of sign in)
```docker
docker push lygioian/ctxh:tagname
```

### **Git**
There are some basic concept of git: commit, branch, tag, clone, pull, push,...

Commit holds the current state of the repository. A commit is also named by SHA1 hash. You can consider a commit object as a node of the linked list. Every commit object has a pointer to the parent commit object. From a given commit, you can traverse back by looking at the parent pointer to view the history of the commit. If a commit has multiple parent commits, then that particular commit has been created by merging two branches.

Branches are used to create another line of development. By default, Git has a master branch, which is same as trunk in Subversion. Usually, a branch is created to work on a new feature. Once the feature is completed, it is merged back with the master branch and we delete the branch. Every branch is referenced by HEAD, which points to the latest commit in the branch. Whenever you make a commit, HEAD is updated with the latest commit.

Tag assigns a meaningful name with a specific version in the repository. Tags are very similar to branches, but the difference is that tags are immutable. It means, tag is a branch, which nobody intends to modify. Once a tag is created for a particular commit, even if you create a new commit, it will not be updated. Usually, developers create tags for product releases.

Clone operation creates the instance of the repository. Clone operation not only checks out the working copy, but it also mirrors the complete repository. Users can perform many operations with this local repository. The only time networking gets involved is when the repository instances are being synchronized.

Pull operation copies the changes from a remote repository instance to a local one. The pull operation is used for synchronization between two repository instances. This is same as the update operation in Subversion.

Push operation copies changes from a local repository instance to a remote one. This is used to store the changes permanently into the Git repository. This is same as the commit operation in Subversion

A staging step in git allows you to continue making changes to the working directory, and when you decide you wanna interact with version control, it allows you to record changes in small commits.

We have some basic command for git, for example: 
```
$ git init
Initialized empty Git repository

#Adds files in the to the staging area for Git. Before a file is available to commit to a repository, the file needs to be added to the Git index (staging area).
$ git add <file or directory name>

# Adding a commit with message
$ git commit -m "Commit message in quotes"

#git status will return the state of current working branch.
$ git status

To determine what branch the local repository is on, add a new branch, or delete a branch.
# Create a new branch
$ git branch <branch_name>

# List all remote or local branches
$ git branch -a

# Delete a branch
$ git branch -d <branch_name>

#To start working in a different branch, use git checkout to switch branches.
# Checkout an existing branch
$ git checkout <branch_name>

# Checkout and create a new branch with that name
$ git checkout -b <new_branch>

#Integrate branches together we use git merge
# Merge changes into current branch
$ git merge <branch_name>

```

Different Between Merge and Rebase

Git rebase and merge both integrate changes from one branch into another. Where they differ is how it's done. Git rebase moves a feature branch into a master. Git merge adds a new commit, preserving the history.

Different Between Reset Hard vs. Soft vs. Mixed
```
reset --soft : History changed, HEAD changed, Working directory is not changed.

reset --mixed : History changed, HEAD changed, Working directory changed with unstaged data.

reset --hard : History changed, HEAD changed, Working directory is changed with lost data
```


### **CI/CD**

With gitlab CI/CD, we create file .gitlab-ci.yml
```yml
#.gitlab-ci.yml
stages:
  - publish
  - production

image: docker:latest

variables:
  TAG_LATEST: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:latest
  TAG_COMMIT: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHORT_SHA

publish:
  image: docker:latest
  stage: publish
  services:
    - docker:dind
  script:
    - docker build -t $TAG_COMMIT -t $TAG_LATEST .
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker push $TAG_COMMIT
    - docker push $TAG_LATEST

production:
  type: deploy
  stage: production
  image: ruby:latest
  script:
    - apt-get update -qy
    - apt-get install -y ruby-dev
    - gem install dpl
    - dpl --provider=heroku --app=testcicd-ctxh --api-key=$HEROKU_API_KEY
  only:
    - master
```
I create 2 job for each time we push a new commit to branch. First publish a docker container to gitlab container registry. Second, it will publish the project into heroku.


### **Linux**

#### **Systemd**
First we create a shell script file name test.sh.
```sh
#!/bin/bash

echo "htg.service: ## Starting ##" | systemd-cat -p info

while :
do
TIMESTAMP=$(date '+%Y-%m-%d %H:%M:%S')
echo "htg.service: timestamp ${TIMESTAMP}" | systemd-cat -p info
sleep 60
done
```
Then we will copy it to /usr/local/bin and make it executable
```
sudo cp test.sh /usr/local/bin
sudo chmod +x /usr/local/bin/test.sh

```
Then we will check our service name by using
```sh
sudo systemctl list-unit-files --type-service
```
Make sure that you will create the unique name of service and then create a test.service in /etc/systemd/system
```sh
sudo vim /etc/systemd/system/test.service
```
```sh
[Unit]
Description=Service Example

[Service]
Type=simple
ExecStart=/usr/local/bin/htg.sh
Restart=on-failure
RestartSec=10 #How long to wait before attempting to restart the service. This value is in seconds.

[Install]
WantedBy=multi-user.target
```
Next we will check the status of the service and start it.
```
# When we add a new unit file or edit an existing one, we must tell systemd to reload the unit file definitions.
sudo systemctl daemon-reload
# First we need to be enable it after start the new service
sudo systemctl enable test
sudo systemctl start test

#Use systemctl status to check the logs of the service
sudo systemctl status test.service

```

#### **1. Printing All Lines**
```
awk '{print $0}' text.txt
```
#### **2. Printing Column in Any Order**
```
awk '{print $1}' text.txt
#if you print $0 mean it choose all col, so $1, $2 it will be a column 1,2. For example you can print exactly multiple columns like:
awk '{print $1 $3}' text.txt
```
#### **3. Printing Column by Pattern**
```
awk '/<pattern>/ {print $0}' test.txt
```
#### **3. Counting and Printing Matched Pattern**
```
awk '/a/{++count} END {print count}' test.txt
```
#### **4. Printing Lines with More than 18 Characters**
```
awk 'length($0) > 18' test.txt
```

